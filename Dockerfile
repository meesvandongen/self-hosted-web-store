FROM node:22 as builder

WORKDIR /app/medusa

COPY . . 

RUN npm install --loglevel=error

RUN npm run build


FROM node:22

WORKDIR /app/medusa

RUN mkdir dist

COPY package*.json ./ 

COPY develop.sh .

RUN ["chmod", "+x", "./develop.sh"]

COPY medusa-config.js .

RUN npm install -g @medusajs/medusa-cli

RUN npm i --only=production

COPY --from=builder /app/medusa/dist ./dist

EXPOSE 9000

ENTRYPOINT ["./develop.sh", "start"]